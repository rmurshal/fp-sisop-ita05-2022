#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 8080

void say_login_error()
{
  char login_error[200];
  bzero(login_error, sizeof(login_error));

  strcpy(login_error, "Login failed, try again with this command: ./client -u [username] -p [password] \n");
  printf("%s", login_error);
  exit(EXIT_FAILURE);
}

int main(int argc, char const *argv[])
{
  /* Variable for auth */
  char username[50];
  bzero(username, sizeof(username));
  char password[50];
  bzero(password, sizeof(password));
  char error[200];
  bzero(error, sizeof(error));
  if (argc == 1 && geteuid() == 0)
  {
    strcpy(username, "root");
    strcpy(password, "root");
  }
  else if (argc == 5)
  {
    if (strcmp(argv[1], "-u") != 0 || strcmp(argv[3], "-p") != 0)
    {
      say_login_error();
    }

    strcpy(username, argv[2]);
    strcpy(password, argv[4]);
  }
  else
  {
    say_login_error();
  }

  /* Setup client socket */
  struct sockaddr_in address;
  int sock = 0, valread;
  struct sockaddr_in serv_addr;
  char buffer[1024] = {0};
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("\n Socket creation error \n");
    return -1;
  }
  memset(&serv_addr, '0', sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);
  if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
  {
    printf("\nInvalid address/ Address not supported \n");
    return -1;
  }
  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("\nConnection Failed \n");
    return -1;
  }

  /* Main Program */
  send(sock, username, strlen(username), 0);
  sleep(1);
  send(sock, password, strlen(password), 0);
  sleep(2);

  valread = read(sock, buffer, 1024);
  printf("Status: %s\n", buffer);

  if (strcmp(buffer, "Login succes") == 0)
  {
    while (1)
    {
      memset(buffer, 0, 1024);
      char cmd[1024];
      printf("Masukkan perintah: ");
      scanf("%[^\n]%*c", cmd);

      send(sock, cmd, strlen(cmd), 0);
      sleep(3);

      valread = read(sock, buffer, 1024);

      if (strstr(cmd, "CREATE USER"))
      {
        printf("%s \n", buffer);
        if (strcmp(buffer, "failedsuccess") == 0)
        {
          printf("User created \n");
        }
        else if (strcmp(buffer, "failedfailed") == 0)
        {
          printf("Failed to create user \n");
        }
      }

      if (strstr(cmd, "USE "))
      {
        // idk why this buffer look like this xD
        if (strcmp(buffer, "failedsuccessfailed") == 0)
        {
          printf("You can use this db \n");
        }
        else if (strcmp(buffer, "failedfailedfailed") == 0)
        {
          printf("You can't use this db \n");
        }
      }

      if (strstr(cmd, "GRANT PERMISSION"))
      {
        // printf("Buffer from server: %s \n", buffer);
        if (strcmp(buffer, "failedsuccess") == 0)
        {
          printf("Permission granted \n");
        }
        else if (strcmp(buffer, "failedfailed") == 0)
        {
          printf("Failed to grant permission \n");
        }
      }

      if (strstr(cmd, "CREATE DATABASE"))
      {
        // printf("Buffer from server: %s \n", buffer);
        if (strcmp(buffer, "success") == 0)
        {
          printf("Create database success \n");
        }
        else if (strcmp(buffer, "failed") == 0)
        {
          printf("Create database failed \n");
        }
      }
    }
  }
  else if (strcmp(buffer, "Login failed") == 0)
  {
    say_login_error();
  }

  return 0;
}

