#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

/* port */
#define PORT 8080

/* variable */
char username[1024] = "";
char password[1024] = "";
char buffer[1024] = "";
char login_success[20] = "Login succes";
char login_failed[20] = "Login failed";
char success_msg[20] = "success";
char failed_msg[20] = "failed";

/* Login */
int login(char username[50], char password[50])
{
  FILE *f = fopen("/home/ramammurshal/fpsisop/database/users/users.txt", "r");
  if (f == NULL)
  {
    printf("Error opening users.txt file \n");
    exit(1);
  }
  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];
  int ind = 0;
  while (fgets(buffer, MAX_LENGTH, f))
  {
    if (ind == 0)
    {
      ind++;
      continue;
    }
    char *token = strtok(buffer, ";");
    if (strcmp(token, username) == 0)
    {
      token = strtok(NULL, ";");

      if (strcmp(token, password) == 0)
      {
        return 1;
      }
    }
  }
  fclose(f);
  return 0;
}

/* Create user */
int create_user(char cmd[50])
{
  char command[20][100];
  memset(command, 0, sizeof(command[0][0]) * 20 * 100);

  int j = 0;
  int ctr = 0;
  for (int i = 0; i <= (strlen(cmd)); i++)
  {
    if (cmd[i] == ' ' || cmd[i] == '\0')
    {
      command[ctr][j] = '\0';
      ctr++; // for next word
      j = 0; // for next word, init index to 0
    }
    else
    {
      command[ctr][j] = cmd[i];
      j++;
    }
  }

  char *char5 = command[5];
  char last_char5 = char5[strlen(char5) - 1];
  char semi_colon = ';';

  if (strcmp(command[0], "CREATE") == 0 && strcmp(command[1], "USER") == 0 && strcmp(command[3], "IDENTIFIED") == 0 && strcmp(command[4], "BY") == 0 && last_char5 == semi_colon)
  {
    char *pass = strtok(command[5], ";");
    FILE *f = fopen("/home/ramammurshal/fpsisop/database/users/users.txt", "a");
    if (f == NULL)
    {
      printf("Error opening users.txt file \n");
      exit(1);
    }

    fprintf(f, "%s;%s\n", command[2], pass);
    fclose(f);

    return 1;
  }
  else
  {
    return 0;
  }
}

/* Use database */
int use_db(char cmd[50], char username[50])
{
  // get db_name from command user
  char *db_name = strtok(cmd, " ");
  char *command_user = db_name;

  db_name = strtok(NULL, " ");
  char *check_db_name = db_name;
  char last_char_db_name = check_db_name[strlen(check_db_name) - 1];
  char semi_colon = ';';

  if (strcmp(command_user, "USE") < 0 || last_char_db_name != semi_colon)
  {
    return 0;
  }

  db_name = strtok(db_name, ";");

  FILE *f = fopen("/home/ramammurshal/fpsisop/database/users/users.txt", "r");
  if (f == NULL)
  {
    printf("Error opening users.txt file \n");
    exit(1);
  }

  const unsigned MAX_LENGTH = 256;
  char buffer[MAX_LENGTH];
  int ind = 0;

  // open f from users.txt
  while (fgets(buffer, MAX_LENGTH, f))
  {
    if (ind == 0)
    {
      ind++;
      continue;
    }
    // get username from that line
    char *db_name_db = strtok(buffer, ";");
    if (strcmp(db_name_db, username) == 0 || strcmp(username, "root") == 0)
    {
      db_name_db = strtok(NULL, ";");
      db_name_db = strtok(NULL, ";");

      // get db permission and loop, return 1 if match
      db_name_db = strtok(db_name_db, ",");
      while (db_name_db != NULL)
      {
        if (strcmp(db_name, db_name_db) == 0)
        {
          return 1;
        }
        db_name_db = strtok(NULL, "\n ,");
      }
    }
  }
  fclose(f);
  return 0;
}

// grant permission
int grant_permission(char buffer[50])
{
  char *token = strtok(buffer, " ");

  // get all command
  char cmd1[10];
  strcpy(cmd1, token);

  token = strtok(NULL, " ");
  char cmd2[20];
  strcpy(cmd2, token);

  token = strtok(NULL, " ");
  char cmd3_db_name[20];
  strcpy(cmd3_db_name, token);

  token = strtok(NULL, " ");
  char cmd4[20];
  strcpy(cmd4, token);

  token = strtok(NULL, " ");
  char cmd5_temp[20];
  strcpy(cmd5_temp, token);
  token = strtok(token, ";");
  char cmd5_username[20];
  strcpy(cmd5_username, token);

  char cmd5_last_char = cmd5_temp[strlen(cmd5_temp) - 1];
  char semi_colon = ';';

  if (strcmp(cmd1, "GRANT") == 0 && strcmp(cmd2, "PERMISSION") == 0 && strcmp(cmd4, "INTO") == 0 && cmd5_last_char == semi_colon)
  {
    // rename
    rename("/home/ramammurshal/fpsisop/database/users/users.txt", "/home/ramammurshal/fpsisop/database/users/temp.txt");

    // open past file
    FILE *f1 = fopen("/home/ramammurshal/fpsisop/database/users/temp.txt", "r");
    if (f1 == NULL)
    {
      printf("Error opening /home/ramammurshal/fpsisop/database/users/temp.txt file \n");
      exit(1);
    }
    const unsigned MAX_LENGTH_F1 = 256;
    char buffer_1[MAX_LENGTH_F1];

    // creating file 2
    FILE *f2 = fopen("/home/ramammurshal/fpsisop/database/users/users.txt", "w");
    if (f2 == NULL)
    {
      printf("Error creating /home/ramammurshal/fpsisop/database/users/users.txt file \n");
      exit(1);
    }

    // updating data
    while (fgets(buffer_1, MAX_LENGTH_F1, f1))
    {
      char temp[50];
      strcpy(temp, buffer_1);
      char *content = strtok(buffer_1, ";");
      if (strcmp(content, cmd5_username) == 0)
      {
        char *remove_n_from_temp = strtok(temp, "\n");
        fprintf(f2, "%s,%s\n", remove_n_from_temp, cmd3_db_name);
      }
      else
      {
        fprintf(f2, "%s", temp);
      }
    }

    remove("/home/ramammurshal/fpsisop/database/users/temp.txt");

    fclose(f1);
    fclose(f2);

    return 1;
  }
  else
  {
    return 0;
  }

  return 0;
}

int create_database(char command[50], char username[50])
{
  char cmd1[20];
  char *token = strtok(command, " ");
  strcpy(cmd1, token);

  char cmd2[20];
  token = strtok(NULL, " ");
  strcpy(cmd2, token);

  char cmd3_temp[20];
  token = strtok(NULL, " ");
  strcpy(cmd3_temp, token);

  char cmd3[20];
  token = strtok(token, ";");
  strcpy(cmd3, token);

  char last_cmd3_char = cmd3_temp[strlen(cmd3_temp) - 1];
  char semi_colon = ';';

  // printf("%s %s %s %s %c \n", cmd1, cmd2, cmd3_temp, cmd3, last_cmd3_char);

  if (strcmp(cmd1, "CREATE") == 0 && strcmp(cmd2, "DATABASE") == 0 && last_cmd3_char == semi_colon)
  {
    // rename
    rename("/home/ramammurshal/fpsisop/database/users/users.txt", "/home/ramammurshal/fpsisop/database/users/temp.txt");

    // open past file
    FILE *f1 = fopen("/home/ramammurshal/fpsisop/database/users/temp.txt", "r");
    if (f1 == NULL)
    {
      printf("Error opening /home/ramammurshal/fpsisop/database/users/temp.txt file \n");
      exit(1);
    }
    const unsigned MAX_LENGTH_F1 = 256;
    char buffer_1[MAX_LENGTH_F1];

    // creating file 2
    FILE *f2 = fopen("/home/ramammurshal/fpsisop/database/users/users.txt", "w");
    if (f2 == NULL)
    {
      printf("Error creating /home/ramammurshal/fpsisop/database/users/users.txt file \n");
      exit(1);
    }

    // updating data
    while (fgets(buffer_1, MAX_LENGTH_F1, f1))
    {
      char temp[50];
      strcpy(temp, buffer_1);
      char *content = strtok(buffer_1, ";");
      if (strcmp(content, username) == 0)
      {
        char *remove_n_from_temp = strtok(temp, "\n");
        // printf("%s,%s\n", remove_n_from_temp, cmd3);
        fprintf(f2, "%s,%s\n", remove_n_from_temp, cmd3);
      }
      else
      {
        // printf("%s", temp);
        fprintf(f2, "%s", temp);
      }
    }

    remove("/home/ramammurshal/fpsisop/database/users/temp.txt");

    fclose(f1);
    fclose(f2);

    return 1;
  }
  else
  {
    // printf("Off \n");
    return 0;
  }

  return 0;
}

int create_table(char command[50], char username[50]){
    char path[1024];
    FILE *table = fopen(path, "w");
    
    char rows[1024];
    int length = strlen(rows);

    for(int i = 0; i < length; i++){
        char line[1024] = strcat(" ", "~");
    }

    sprintf("%s\n%s\n%s\n", line, rows, line);
    fputs(table);
    fclose(table);

    return 0;
}


int main()
{
  /* ===== Setup Socket ===== */
  int server_fd, new_socket, valread;
  struct sockaddr_in address;
  int opt = 1;
  int addrlen = sizeof(address);
  char buffer[1024] = {0};
  char *hello = "Hello from server secondary";
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
  {
    perror("socket failed");
    exit(EXIT_FAILURE);
  }
  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
  {
    perror("setsockopt");
    exit(EXIT_FAILURE);
  }
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);
  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
  {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }
  if (listen(server_fd, 3) < 0)
  {
    perror("listen");
    exit(EXIT_FAILURE);
  }

  /*===== Setup Daemon ======*/
  pid_t pid, sid; // Variabel untuk menyimpan PID
  pid = fork();   // Menyimpan PID dari Child Process
  /* Keluar saat fork gagal
   * (nilai variabel pid < 0) */
  if (pid < 0)
  {
    exit(EXIT_FAILURE);
  }
  /* Keluar saat fork berhasil
   * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0)
  {
    exit(EXIT_SUCCESS);
  }
  umask(0);
  sid = setsid();
  if (sid < 0)
  {
    exit(EXIT_FAILURE);
  }
  if ((chdir("/")) < 0)
  {
    exit(EXIT_FAILURE);
  }
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  while (1)
  {
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
      perror("accept");
      exit(EXIT_FAILURE);
    }

    valread = read(new_socket, username, 1024);
    valread = read(new_socket, password, 1024);

    if ((strcmp(username, "root") == 0 && strcmp(password, "root") == 0) || login(username, password))
    {
      memset(buffer, 0, 1024);
      send(new_socket, login_success, strlen(login_success), 0);

      while (1)
      {
        memset(buffer, 0, 1024);
        valread = read(new_socket, buffer, 1024);
        int success = 0;

        if (strstr(buffer, "CREATE USER") && (strcmp(username, "root") == 0 && strcmp(password, "root") == 0))
        {
          success = create_user(buffer);
        }
        else
        {
          success = 0;
        }

        if (strstr(buffer, "USE "))
        {
          success = use_db(buffer, username);
        }

        if (strstr(buffer, "GRANT PERMISSION") && (strcmp(username, "root") == 0 && strcmp(password, "root") == 0))
        {
          success = grant_permission(buffer);
        }
        else
        {
          success = 0;
        }

        if (strstr(buffer, "CREATE DATABASE") && (strcmp(username, "root") != 0 && strcmp(password, "root") != 0))
        {
          success = create_database(buffer, username);
        }
        else
        {
          success = 0;
        }

        if (strstr(buffer, "CREATE TABLE") && (strcmp(username, "root") != 0 && strcmp(password, "root") != 0))
        {
          success = create_table(buffer, username);
        }
        else
        {
          success = 0;
        }

        // send response to user
        if (success == 1)
        {
          send(new_socket, success_msg, strlen(success_msg), 0);
        }
        else if (success == 0)
        {
          send(new_socket, failed_msg, strlen(failed_msg), 0);
        }
      }
    }
    else
    {
      send(new_socket, login_failed, strlen(login_failed), 0);
    }

    sleep(30);
  }
}

